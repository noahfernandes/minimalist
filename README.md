# minimalist

A minimalistic design to a todo application. A nice feature is that tasks automatically sort themselves based on it's priority and the amount of effort needed to complete it. There's also a little space to write a descriptive note for a task.

Currently, the application uses the browser's local storage to store data. In the near future, there are plans to connect it with a real database.

[Go to the app](https://minimalist.noahdecoco.com)

## frontend development
The app is build in Vue. To start the local dev server:
* `cd client`
* `yarn install`
* `yarn serve`
