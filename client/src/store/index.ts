import Task from "@/models/task";
import LocalStorage from "@/utilities/local-storage";
import Vue from "vue";
import Vuex, { ActionTree, GetterTree, MutationTree, StoreOptions } from "vuex";
import TaskService from "@/services/TaskService";

Vue.use(Vuex);

interface StateInterface {
  tasks: Task[];
  selectedTask: null | Task;
}

const state: StateInterface = {
  tasks: [],
  selectedTask: null
};

const getters: GetterTree<StateInterface, any> = {
  tasks(state: StateInterface): Task[] {
    return state.tasks;
  },
  selectedTask(state: StateInterface): Task | null {
    return state.selectedTask;
  }
};

const actions: ActionTree<StateInterface, any> = {
  getTasks(context): void {
    const tasks = LocalStorage.getItem("tasks");
    if (tasks) {
      context.commit("setTasks", tasks);
    }
  },
  createTask(context, { rawInput }: { rawInput: string }): void {
    const taskData = TaskService.extractDataFromText(rawInput);
    const tasks = [...context.state.tasks, new Task(taskData)];
    context.commit("setTasks", tasks);
    LocalStorage.setItem("tasks", tasks);
  },
  deleteTask(context, id: string): void {
    const tasks = context.state.tasks.filter((task: Task) => task.id != id);
    context.commit("setTasks", tasks);
    LocalStorage.setItem("tasks", tasks);
  },
  updateTask(context, updatedTask: Task): void {
    const tasks = context.state.tasks.map((task: Task) => {
      return task.id === updatedTask.id ? updatedTask : task;
    });
    context.commit("setTasks", tasks);
    LocalStorage.setItem("tasks", tasks);
  }
};

const mutations: MutationTree<StateInterface> = {
  setTasks(state: StateInterface, tasks: Task[]): void {
    state.tasks = tasks.sort((a: any, b: any) => {
      if (a.isCompleted) return 1;
      if (b.isCompleted) return -1;
      return b.priority - a.priority || a.effort - b.effort;
    });
  },
  setSelectedTask(state: StateInterface, task: Task): void {
    state.selectedTask = task;
  }
};

const storeOptions: StoreOptions<StateInterface> = {
  state,
  getters,
  actions,
  mutations
};

export default new Vuex.Store(storeOptions);
